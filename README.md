# Data Gathering from Freund 2017

Coexistence of potent HIV-1 broadly neutralizing antibodies and antibody-sensitive viruses in a viremic controller.
Natalia T. Freund et al. 
Sci. Transl. Med. 9, eaal2144 (2017).
<https://doi.org/10.1126/scitranslmed.aal2144>

For related amino acid sequences see BG18 entries at
<https://www.ncbi.nlm.nih.gov/protein> from
[10.1126/science.aax4380](https://doi.org/10.1126/science.aax4380) and
[10.1038/s41467-018-03632-y](https://doi.org/10.1038/s41467-018-03632-y).
